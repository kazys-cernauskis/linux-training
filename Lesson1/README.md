# Pirma paskaita

## Temos:
 - Darbo vietos pasiruošimas (hyper-v, vagrant)
 - Pirmi veiksmai su namų aplinka (home dir, ls, pwd)
 
### Darbo vietos pasiruošimas
Reikalavimai:
  - [Hyper-v](#Hyper-v)
  - [Vagrant](#Vagrant)
  - [Vagrantfile](#Vagrantfile)

#### Hyper-v
Microsoft virtualizacijos serviso diegimas
Administratoriau tesiėmis Atsidarykite PowerShell konsolę ir įvykdykit šią komandą:
```powershell
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

#### Vagrant
Virtualios mašinos automatizavimo įrankis - [Vagrant](https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.msi)
Atsisiūsti ir susidiegti

#### Vagrantfile
Automatizuotas Centos 7 VM sukūrimas
 - Susikurkite darbinę direktoriją
 - Atsisūskite [Vagrantfile] į darbinę direktoriją
```powershell
Invoke-WebRequest "https://Some-url.com/Vagrantfile" -OutFile ".\Vagrantfile"  
```

#### Darbo aplinkos paleidimas
Darbo direktorijoje su powershell terminale leidžiame komanda:
```powershell
Vagrant.exe up
```

Laukiame kol Virtuali Linux aplinka bus paruošta

Prisijungiame prie aplinkos:
```powershell
Vagrant.exe ssh
```
Išėjimas iš aplinkos:
```bash
exit
```
Arba klaviatūros kombinacija [CTRL+X]

##### Kitos komandos

Virtualios aplinkos sustabdymas. Virtuali aplinka sustaboma taupant kompiuerio resursus:
```powershell
Vagrant.exe stop
```

Virtualios aplinkos atstatymas po sustabdymo:
```powershell
Vagrant.exe up --provision
```

Virtualios aplinkos sunaikinimas. Sustabodma aplinka ir sunaikinamas diskas. Buvusios aplinkos atsatyti nebegalima:
```powershell
Vagrant.exe destroy
```
### Darbas su namų aplinka (HomeDir)
Linux komandinės eilutės funkcionalumą sukuria bedrai vadinama programa `shell`. Kiekviena distribucija naudoja skirtingus shell interpretatorius:
 - zsh - MacOS <sup>[1]</sup>
 - ash - Alpinelinux <sup>[2]</sup>
 - bash - Debian/RedHat family os

#### Bash eilutės aprašymas

TBD

#### Bash komandų vykdymas ir pagalbininkai

man aprasymas

tail, head, wc, ls, date, file komandu pavyzdziai

TAB pagalbos pavyzdziai

History komanda

Praktika


### Operacijos su failais

#### Failu sistemos hierarchija

Teorija

#### Navigacija

Absolute path

Relative path

Komandos:
 - cd
 - pwd
 - ls
 - touch

 Praktika

#### Failu operacijos

Komandos:
 - mv
 - cp
 - rm
 - mkdir

 Praktika

 #### Kintamieji failu operacijose

 Komandos:
 - mv
 - cp
 - rm
 - mkdir



[1]: https://www.theverge.com/2019/6/4/18651872/apple-macos-catalina-zsh-bash-shell-replacement-features
[2]: https://www.theverge.com/2019/6/4/18651872/apple-macos-catalina-zsh-bash-shell-replacement-features
