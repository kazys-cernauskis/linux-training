# Linux trainings
Links:
 - [**Paskaita #1**](Lesson1/README.md):
   - Darbo vietos pasiruošimas
   - Komandinės eilutės pradmenys
   - Komandų instrukcijos (man, info, /usr/share/docs)
   - Navigacija failų sistemoje
   - Operacijos su failais (cp, mv, rm)
   - Kintamieji failų operacijose ( `{1..3}`,*,$)

 - [**Paskaita #2**](Lesson2/README.md):
   - Išvesties nukreipimai (stdout, pipe, tee, redirect)
   - Teksto redagatoriai (vim, nano)
   - Failu kompresavimas (tar)
   - Failų paieška (locate, find)
   - hard and soft links (ln)
   - crontab

 - **Paskaita #3** - Vartotojai, Grupės ir Teisės:
   - Apžvalga (id, /etc/passwd, /etc/group, UID, GID)
   - Super privilegijos (su, sudo)
   - Vartotojų administravimas (useradd, usermod, userdel, passwd)
   - Grupių administravimas (grouadd, groupmod, groupdel)
   - Failų teisės (chmod,chown,)

 - **Paskaita #4** - Procesai
   - Apžvalga
   - Užduočių valdymas (jobs,&,fg,bg)
   - KILL signalai (kill, killall, pkill, pstree, w)
   - Procesu metrikos (top, uptime, w, ps, nice)

 - **Paskaita #5**
   - Servisai ir Demonai
     - Systemd Apžvalga
     - Servisu valdymas su systemctl
   - SSH operacijos
     - SSH standartinis prisijungimas
     - SSH prisijungimas su raktu
     - SSHD konfiguravimas teisiu apribojimui
     - Failu kopijavimas tarp sistemu (scp, rsync)

 - **Paskaita #6** 
   - Logai
      - Loginimo mechanizmo apžvalga (systemd-journal, rsyslog)
      - Syslog pranešimų atributai
      - Logų rotavimas
      - rsyslog konfiguravimas
      - Logų stebėjimas ir išsiuntimas (tail, logger)
      - Journalctl naudojimas
    - Laiko servisas
      - Laiko zonos keitimas
      - Laiko sinchronizavimas

 - **Paskaita #7** - Paketų operacijos
   - Paketų operacijų apžvalga (yum, rpm, repo)
   - Yum operacijos
   - Yum ir repo konfiguracijos
   - RPM operacijos (rpm, rpm2cpio)

 - **Paskaita #8** - Teksto manipuliavimai
   - Grep komanda
   - Sed komanda
   - AWK įrankis
   - Bash skriptai

 - **Paskaita** #9 - Disko ir Failų Sistemos (FS) operacijos
   - Diskų ir FS apžvalga (du, df, /dev/)
   - Disko prijungimas prie hierarchijos (mount, fstab, blkid, lsof)
   - Disko paruošimas ir formatavimas (dd, fdisk, mkfs, swap)

 - **Paskaita** #10 - Tinklo konfiguravimas:
   - Hostname keitimas
   - Hosts ir resolv.conf failai
   - network-scripts konfiguravimas (Bond, vlan, routes, pre-up, post-down)
   - firewalld





